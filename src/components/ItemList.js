import styles from '../styles/ItemList.module.css'
import { Item } from "./Item";

export function ItemList({ lista }) {

    return (
        <div className={styles.contenedor}>
            {lista.map((prod) => <Item prod={prod} key={prod.id} />)}
        </div>
    )
}