import { useContext, useState } from "react"
import { CartContext } from "../context/CartContext"
import { collection, addDoc } from "firebase/firestore"
import { db } from "../firebase/config"

export function CheckOut() {

    const { cart, cartTotal, emptyCart } = useContext(CartContext)

    const [values, setValues] = useState({
        nombre: '',
        email: '',
        tel: ''
    })

    const [process, setProcess] = useState("process")

    const[orden, setOrden] = useState()

    const handleInputChange = (e) => {
        setValues({
            ...values,
            [e.target.name]: e.target.value
        }
        )
    }

    const handleSubmit = (e) => {
        setProcess("inProcess")
        const orden = {
            items: cart,
            total: cartTotal(),
            comprador: { ...values }
        }

        const ordersRef = collection(db, 'orders')
        addDoc(ordersRef, orden)
            .then((doc) => {
                setOrden(doc.id)
            }).finally(() => {
                emptyCart()
                setProcess("finally")
            }).error(() => {
                setProcess("error")
            })
    }
    return (
        <div>
            <h2>checkOut</h2>
            <hr />
            {process == "process" &&
                <>
                    <input
                        type='text'
                        placeholder="Tu nombre"
                        value={values.nombre}
                        name='nombre'
                        onChange={handleInputChange}
                        className="App-text"
                        required
                    />
                    <br/>
                    <input
                        type='email'
                        placeholder="Tu email"
                        value={values.email}
                        name="email"
                        onChange={handleInputChange}
                        className="App-text"
                        required
                    />
                    <br/>
                    <input
                        type='tel'
                        placeholder='Telefono'
                        value={values.tel}
                        name="tel"
                        onChange={handleInputChange}
                        className="App-text"
                        required
                    />
                    <br/>
                    <button onClick={() => handleSubmit()} className="App-button">Enviar</button>
                </>
            }

            {process == "finally" &&
                <>
                    <h3>Su pedido fue realizado con exito, su numero de orden es el {orden}</h3>
                </>
            }
            {process == "inProcess" &&
                <>
                    <h3>Procesando su pedido</h3>
                </>
            }
            {process == "error" &&
                <>
                    <h3>No se pudo procesar su pedido</h3>
                </>
            }
        </div>
    )
}