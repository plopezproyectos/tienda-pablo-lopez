import { useContext } from "react";
import { Link } from "react-router-dom";
import { CartContext } from "../context/CartContext";

const Cart = () => {
    const { cart, cartTotal, emptyCart, removeItem, cartQuantity } = useContext(CartContext)
    const remove = function (params) {
        removeItem(params)
    }
    return (
        <div className="container my-5">
            <h2>Tu compra</h2>
            <hr />
            {
                cart.map((item) => (
                    <>
                        <table>
                            <tr>
                                <td>
                                    <img src={item.pictureUrl} />
                                </td>
                                <td>
                                    <div key={item.id}>
                                        <h4>{item.title}</h4>
                                        Cantidad: {item.cantidad}
                                        <h5>Precio: ${item.price * item.cantidad}</h5>
                                        <button onClick={() => remove(item.id)} className="App-button">borrar</button>

                                    </div>
                                </td>
                            </tr>
                        </table>
                        <hr />
                    </>
                ))

            }
            <h3>Total: ${cartTotal()}</h3>
            <hr />
            <button onClick={emptyCart} className="App-button">Vaciar carrito</button>

            {cartQuantity() != "0" &&
                <Link to={"/checkout"}><button className="App-button2">Finalizar compra</button></Link>
            }
        </div>
    )

}

export default Cart