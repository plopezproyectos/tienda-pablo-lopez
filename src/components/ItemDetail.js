import { useEffect, useState } from "react"
import styles from '../styles/ItemDetail.module.css'
import { ItemCount } from "./ItemCount";

export function ItemDetail(producto) {

    const { id, title, description, price, pictureUrl, stock, category } = producto
    const [mostrar, setMostrar] = useState(false)

    useEffect(() => {
        setTimeout(() => {
            setMostrar(true)
        }, 2000);
    }, [id]);
    return (
        <div className={styles.contenedor}>
            {mostrar ? (
                <table>
                    <tr>
                        <td>
                            <img src={pictureUrl} className={pictureUrl} />
                        </td>
                        <td>
                            <h3>{title}</h3>
                            <h4>$ {price}</h4>
                            <h5>{description}</h5>
                            <ItemCount producto={producto} initial="1" />
                        </td>
                    </tr>
                </table>
            ) : (
                <div>Cargando...</div>
            )
            }
        </div>
    )
}