import styles from '../styles/Item.module.css'
import { ItemCount } from "./ItemCount"
import { Link } from "react-router-dom"

export function Item({ prod }) {
    return (
        <div className={styles.contenedor}>
            <img src={prod.pictureUrl} className={styles.img} />
            <div>
                <div className={styles.title}>{prod.title}</div>
                <div className={styles.price}>Precio $ {prod.price}</div>
            </div>
            <ItemCount producto={prod} initial="1" />
            <Link className="App-link" to={`/item/${prod.id}`}>Ver Mas</Link>
        </div>
    )
}
