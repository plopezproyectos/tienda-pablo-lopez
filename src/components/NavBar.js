import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';
import { Link } from 'react-router-dom';

const settings = ['Perfil', 'Cerrar sesion'];
const category = ['remeras', 'buzos', 'zapatillas'];

const NavBar = () => {
    const [anchorElNav, setAnchorElNav] = React.useState(null);
    const [anchorElUser, setAnchorElUser] = React.useState(null);
    const [anchorElCategory, setAnchorElCategory] = React.useState(null);

    const handleOpenNavMenu = (event) => {
        setAnchorElNav(event.currentTarget);
    };
    const handleOpenUserMenu = (event) => {
        setAnchorElUser(event.currentTarget);
    };

    const handleOpenCategoryMenu = (event) => {
        setAnchorElCategory(event.currentTarget);
    };

    const handleCloseNavMenu = () => {
        setAnchorElNav(null);
    };

    const handleCloseUserMenu = () => {
        setAnchorElUser(null);
    };

    const handleCloseCategoryMenu = () => {
        setAnchorElCategory(null);
    };

    return (
        <AppBar position="static" color="transparent">
            <Container maxWidth="xl">

                <Toolbar disableGutters sx={{ justifyContent: "space-between" }}>
                    <Typography
                        variant="h6"
                        noWrap
                        component="div"
                        sx={{ mr: 2, display: { xs: 'none', md: 'flex' } }}
                    >
                        Tienda López
                    </Typography>

                    <Box sx={{ mr: 2, display: { xs: 'flex', md: 'none' }, position: 'initial!important' }}>
                        <IconButton
                            size="large"
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            onClick={handleOpenNavMenu}
                            color="inherit"
                            sx={{ p: 0, position: 'initial!important' }}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Menu
                            id="menu-appbar"
                            anchorEl={anchorElNav}
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'left',
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'left',
                            }}
                            open={Boolean(anchorElNav)}
                            onClose={handleCloseNavMenu}
                            sx={{
                                display: { xs: 'block', md: 'none' },
                            }}
                        >
                            <MenuItem
                                open={Boolean(anchorElNav)}
                                onClose={handleCloseNavMenu}
                                onClick={handleOpenCategoryMenu}
                            >
                                <Typography textAlign="center">Categorias</Typography>
                            </MenuItem>

                        </Menu>
                    </Box>
                    <Typography
                        variant="h6"
                        noWrap
                        component="div"
                        sx={{ mr: 2, display: { xs: 'flex', md: 'none' } }}
                    >
                        Tienda López
                    </Typography>
                    <Box sx={{ mr: 2, display: { xs: 'none', md: 'flex', position: 'initial!important' } }}>
                        <Button
                            onClick={handleCloseNavMenu}
                            sx={{ my: 2, color: 'black', display: 'block', position: 'initial!important' }}
                            onClose={handleCloseNavMenu}
                            onClick={handleOpenCategoryMenu}
                        >
                            CATEGORIAS
                        </Button>

                    </Box>
                    <Box sx={{ mr: 2, position: 'initial!important' }}>

                        <Menu
                            sx={{ mt: '5px', ml: "110px" }}
                            id="menu-appbar"
                            anchorEl={anchorElCategory}
                            anchorOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            open={Boolean(anchorElCategory)}
                            onClose={handleCloseCategoryMenu}
                        >
                            {category.map((cate) => (
                                <MenuItem key={cate} onClick={handleCloseCategoryMenu}>
                                    <Link className="App-link" to={`/category/${cate}`}><Typography textAlign="center">{cate}</Typography></Link>
                                </MenuItem>
                            ))}
                        </Menu>
                    </Box>
                </Toolbar>
            </Container>
        </AppBar>
    );
};
export default NavBar;
