import ShoppingCartCheckoutIcon from '@mui/icons-material/ShoppingCartCheckout';
import { useContext } from 'react';
import { Link } from 'react-router-dom';
import { CartContext } from '../context/CartContext';

export function CartWidget({ children }) {

    const { cartQuantity } = useContext(CartContext)

    return (
        <Link to={"/cart"} className="cart-widget" >
            <ShoppingCartCheckoutIcon />
            <span>{cartQuantity()} <br />ir al carrito</span>

        </Link>
    )

}