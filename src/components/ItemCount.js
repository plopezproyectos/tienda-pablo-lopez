import { Alert } from '@mui/material';
import { useContext, useEffect, useState } from 'react';
import { CartContext } from '../context/CartContext';
import { CartWidget } from './CartWidget';

export function ItemCount({ producto, initial }) {

    const { id, title, description, price, pictureUrl, stock, category } = producto
    const [cantidad, setCantidad] = useState(Number(initial));

    useEffect(() => {
        if (initial === undefined) {
            initial = 1;
        }
    }, [])

    const { addItem, isInCart } = useContext(CartContext)

    const onAdd = function (quantityOnAdd) {
        if (cantidad > 0) {
            let enviar = { ...producto, "cantidad": quantityOnAdd }
            addItem(enviar)
        }
    }

    const [limiteStock, SetLimiteStock] = useState("")

    function cambiarCantidad(dato) {
        if (dato == "sumar") {
            if (cantidad == stock) {
                SetLimiteStock("maximo")
            } else {
                SetLimiteStock("")
                setCantidad(cantidad + 1)
            }
        } else if (dato == "restar") {
            if (cantidad == 0) {
                SetLimiteStock("minimo")
            } else {
                SetLimiteStock("")
                setCantidad(cantidad - 1)
            }
        }
    }
    return (
        <>
            {
                !isInCart(id) ?
                    <div style={{ color: "blue", fontSize: "20px", padding: "20px", display: "flex", flexWrap: "wrap" }}>
                        <button style={{ width: "40px", height: "40px", marginRight: "5px" }} className="App-button" name="restar" onClick={event => cambiarCantidad(event.target.name)}>-</button>
                        <div style={{ fontSize: "30px", margin: "5px" }}> {"  "}{cantidad}{"  "}</div>
                        <button style={{ width: "40px", height: "40px", marginRight: "5px" }} className="App-button" name="sumar" onClick={event => cambiarCantidad(event.target.name)}>+</button>
                        {limiteStock === "maximo" &&
                            <>
                                <br />
                                <Alert severity="error">stock maximo.</Alert>
                                <br />
                            </>
                        }
                        {limiteStock === "minimo" &&
                            <>
                                <br />
                                <Alert severity="error">No puede comprar negativo</Alert>
                                <br />
                            </>

                        }
                        <br />
                        <button onClick={event => onAdd(cantidad)} className="App-button">Agregar al Carrito</button>
                    </div>
                    :
                    <>
                        <CartWidget>
                            <h3>Ir al carrito</h3>
                        </CartWidget>
                    </>
            }
        </>
    )

}

