import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { ItemDetail } from "./ItemDetail";
import { collection, getDocs } from "firebase/firestore"
import { db } from "../firebase/config.js"

export function ItemDetailContainer() {

    const [producto, setProducto] = useState()
    const { productoId } = useParams()

    useEffect(() => {
        const productosRef = collection(db, "productos")
        //No se porque no anda y los videos y imagenes de la clase firestone estan caidos en la plataforma
        // const q = productoId ? query(productosRef, where('id','==',productoId)) : productosRef
        //llamar async a esa referencia
        getDocs(productosRef)
            .then(resp => {
                const items = resp.docs.map((doc) => doc.id == productoId && setProducto({ id: doc.id, ...doc.data() }))
            })
    }, [productoId])
    return (
        <div style={{ color: "blue", textAlign: "center", fontSize: "20px", width: "100%", padding: "20px" }}>
            <ItemDetail {...producto} />
        </div>
    )
}