import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { ItemList } from "./ItemList";
import { collection, getDocs, query, where } from "firebase/firestore"
import { db } from "../firebase/config.js"

export function ItemListContainer(){

const [lista, setLista] = useState([])

const { categoryId } = useParams()

useEffect( () => {

//armar la referencia
const productosRef = collection(db, "productos")
const q = categoryId ? query(productosRef, where('category','==',categoryId)) : productosRef
//llamar async a esa referencia
getDocs(q)
    .then(resp => {
        const items = resp.docs.map((doc) => ({id: doc.id, ...doc.data()}) )
        console.log(items)
        setLista(items)
    }).finally(() =>{
        console.log("cargo")
    })
}, [categoryId])


    return(    
        <div 
        sx={{ width:"100%", padding: "20px" }}
        >
             <ItemList lista = {lista}/> 
            <br/>
        </div>    
    )    
}