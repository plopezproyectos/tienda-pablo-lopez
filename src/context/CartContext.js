import { createContext, useState } from "react";

export const CartContext = createContext();

export const CartProvider = ({children}) => {

    const [cart, setCart] = useState([])

    const addItem = (item) => {

      console.log("El item es: " + item)

      setCart([...cart, item])
      console.log("El cart es: ")

      setTimeout(() => {
        console.log("El cart contiene: ")
        console.log(cart) 
      }, 3000);
      
    }
    const isInCart = (id) =>{
      return cart.some(prod => prod.id === id)//some devuelte true o false
    }
  
    const cartQuantity = () => {
      return cart.reduce((acc, prod) => acc += prod.cantidad, 0)
    }

    const cartTotal = () => {
        return cart.reduce((acc, prod) => acc += prod.price * prod.cantidad, 0)
    }

    const emptyCart = () =>{
        setCart([])
    }

    const removeItem = (id) => {
        setCart( cart.filter((prod) => prod.id !== id))
    }
    return (
        <CartContext.Provider value = {{
            cart,
            addItem,
            isInCart,
            cartQuantity,
            cartTotal,
            emptyCart,
            removeItem
        }}>
            {children}    
        </CartContext.Provider>
    )

}