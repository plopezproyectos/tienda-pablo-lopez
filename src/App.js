import './App.css';
import { ItemDetailContainer } from './components/ItemDetailContainer';
import { ItemListContainer } from './components/ItemListContainer';
import NavBar from './components/NavBar.js'
import {BrowserRouter, Routes, Route, Navigate} from 'react-router-dom';
import { CartProvider } from './context/CartContext';
import Cart from './components/cart';
import { CheckOut } from './components/CheckOut';

function App() {
 
  return (
    <CartProvider>
             
      <BrowserRouter>
 <NavBar />
        <Routes>
          <Route path='/' element={ <ItemListContainer/> } />
          <Route path='/item/:productoId' element={ <ItemDetailContainer /> } />
          <Route path='/category/:categoryId' element={ <ItemListContainer /> } />
          <Route path='/cart' element={ <Cart /> } />
          <Route path='/checkout' element={ <CheckOut />} />
          <Route path='*' element= { <Navigate to="/"/>}/>
        </Routes>
      </BrowserRouter>
    </CartProvider>
  );
}

export default App;
