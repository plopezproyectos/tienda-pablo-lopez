// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore"
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDgF9P3sCpDydqZzJW3pmx8s00q0OCqiCk",
  authDomain: "tienda-pablo-lopez.firebaseapp.com",
  projectId: "tienda-pablo-lopez",
  storageBucket: "tienda-pablo-lopez.appspot.com",
  messagingSenderId: "592573151752",
  appId: "1:592573151752:web:3402262387dfb4cfe62fbd"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const db = getFirestore(app)